from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import MovieSerializer
from .models import Movie


class MovieView(APIView):

    def get(self, request, pk=None):
        if pk:
            movie = get_object_or_404(Movie.objects.all(), id=pk)
            serializer = MovieSerializer(movie)
            return Response({"movie": serializer.data})
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Response({"list": serializer.data})

    def post(self, request):
        movie = request.data.get('movie')
        serializer = MovieSerializer(data=movie)
        if serializer.is_valid():
            movie_saved = serializer.save()
            return Response({"movie": {
                "id": movie_saved.id,
                "title": movie_saved.title,
                "year": movie_saved.year,
                "director": movie_saved.director,
                "length": movie_saved.length,
                "rating": movie_saved.rating,
            }})
        return my_error_messages(serializer)

    def put(self, request, pk):
        saved_movie = get_object_or_404(Movie.objects.all(), id=pk)
        data = request.data.get('movie')
        serializer = MovieSerializer(instance=saved_movie, data=data, partial=True)
        if serializer.is_valid():
            movie_saved = serializer.save()
            return Response({"movie": {
                    "id": movie_saved.id,
                    "title": movie_saved.title,
                    "year": movie_saved.year,
                    "director": movie_saved.director,
                    "length": movie_saved.length,
                    "rating": movie_saved.rating,
                }})
        return my_error_messages(serializer)

    def delete(self, request, pk):
        movie = get_object_or_404(Movie.objects.all(), id=pk)
        movie.delete()
        return Response(status=202)


def my_error_messages(serializer):
    """Get error message"""
    for key in serializer.errors:
        error_message = serializer.errors[key][0]
    return Response({"status": 500, "reason": f'{error_message}'}, status=500)
