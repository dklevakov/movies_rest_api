from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=100)
    year = models.IntegerField(validators=[MinValueValidator(1900, message='Available values are not less than 1900'),
                                           MaxValueValidator(2100, message='Available values no more than 2100')])
    director = models.CharField(max_length=100)
    length = models.TimeField(auto_now=False, auto_now_add=False)
    rating = models.IntegerField(validators=[MinValueValidator(0, message='Available values are not less than 0'),
                                             MaxValueValidator(10, message='Available values no more than 10')])

    def __str__(self):
        return self.title


