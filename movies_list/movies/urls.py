from django.urls import path
from .views import MovieView

app_name = 'movies'
urlpatterns = [
    path('', MovieView.as_view()),
    path('<int:pk>/', MovieView.as_view()),
]

