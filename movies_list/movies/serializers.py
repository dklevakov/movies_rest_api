from django.core.validators import MinValueValidator, MaxValueValidator
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Movie

default_title_errors = {'max_length': "Ensure 'title' field has no more than 100 characters.",
                        'required': "Field 'title' is required."}

default_director_errors = {'max_length': "Ensure 'director' field has no more than 100 characters.",
                           'required': "Field 'director' is required."}


class MovieSerializer(serializers.Serializer):
    id = serializers.IntegerField(validators=[UniqueValidator(queryset=Movie.objects.all(),
                                                              message="This ID already exists")])
    title = serializers.CharField(max_length=100, allow_blank=False, error_messages=default_title_errors)
    year = serializers.IntegerField(
        validators=[MinValueValidator(1900, message="Field 'year' must be greater than 1900."),
                    MaxValueValidator(2100, message="Field 'year' should be less then 2100.")])
    director = serializers.CharField(max_length=100, allow_blank=False, error_messages=default_director_errors)
    length = serializers.TimeField()
    rating = serializers.IntegerField(
        validators=[MinValueValidator(0, message="Field 'rating' must be greater than 0."),
                    MaxValueValidator(10, message="Field 'rating' should be less then 10.")])

    def create(self, validated_data):
        return Movie.objects.create(**validated_data)

    def update(self, instance, data):
        instance.title = data.get('title', instance.title)
        instance.year = data.get('year', instance.year)
        instance.director = data.get('director', instance.director)
        instance.length = data.get('length', instance.length)
        instance.rating = data.get('rating', instance.rating)
        instance.save()
        return instance

